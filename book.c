#include<stdio.h>
#include<stdlib.h>

typedef int bool_t;

typedef struct book
{
    char book_name[20];
    char author[20];
    int pages;
    float price;
}book_t;

typedef struct node{
    book_t data;
    struct node *next;
}node_t;

typedef struct {
    node_t *head;
}list_t;


enum menu{
    EXIT=0 , PUSH , POP , TOP 
};
void accpt_record(book_t *details);
void print_record(book_t *details);

bool_t is_stack_empty(list_t *b);
bool_t is_stack_full(list_t *b);
void pop(list_t *b,book_t *p);
void init_stack(list_t *b);
node_t *create_node(book_t *p);
void push(list_t *b , book_t *p);
void display_stack(list_t *b);
int menu(void){
    int choies;
    printf("******SLLL*****");
    printf("0.exit\n");
    printf("1.push\n");
    printf("2.pop\n");
    printf("3.top\n");


    printf("ENTER THE CHOIES\n");
    scanf("%d",&choies);

    return choies;

}
int main (void)
{
    list_t b;
    book_t bk;

        while( 1 )
        {
            int choies=menu();
            switch(choies)
            {
                case EXIT:
                    exit(0);
                case PUSH:
                    accpt_record(&bk);
                    push(&b,&bk);
                    print_record(&bk);

                    break;

                case POP:
                
                pop(&b,&bk);
                print_record(&bk);
                    break;
            }
        }
        
    return 0;

}
void accpt_record(book_t *details){

    printf("enter book name\n");
    scanf("%s",details->book_name);
    
    printf("enter  author name\n");
    scanf("%s",details-> author);
   
    printf("enter number of pages\n");
    scanf("%d",&details->pages);
   
    printf("enter the price of the book\n");
    scanf("%f",&details->price);
}
void print_record(book_t *details){

    printf("%-20s  %-20s   %-10d  %-10.2f",details->book_name,details->author,details->pages,details->price);
}


node_t *create_node(book_t *p){
    node_t *temp=(node_t *)malloc(sizeof(node_t));
    if(temp==NULL)
    {
        perror("malloc() failed\n");
        exit(1);
    }
    temp->data = *p;
    temp->next =NULL;
    return temp;
}

void push(list_t *b , book_t *p){

    node_t *newnode=create_node(p);

    if(is_stack_empty(b))
    {
        b->head=newnode;
    }
    else{
        printf("stack_is full\n");
    }

}

void pop(list_t *b,book_t *p){

    if("!is_stack_empty(b"){
        if(b->head==NULL){
            free(b->head);
            b->head=NULL;
        }

    }
}

void init_stack(list_t *b){

    b->head=NULL;
}
bool_t is_stack_empty(list_t *b){

    return(b->head==NULL);
}

bool_t is_stack_full(list_t *b){

    return(b->head->next==NULL);
}
